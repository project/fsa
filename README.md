# FSA - Fastly Streamline Access

This module provides a simple way of adding IP addresses to Fastly ACLs.

The mechanism is simple - a user logs in, or visits, `/user`, and the module will attempt to add their IP address to the ACL targeted in `fastly_streamline_access.settings`.

## Installation

TBD

## Setup

Visit `/admin/config/development/fastly_streamline_access` to set up the module.

### Settings

On the administration page you will find the following settings

- **Standard Access Control List (ACL) name** - this is where the present module will record all IP addresses. The value of this setting should correspond to the ACL name.
- **Long lived Access Control List (ACL) name:** - this is where the admin module will record all IP addresses manually tagged for longer TTLs that average. The value of this setting should correspond to the ACL name.
- **Fastly Service Id** - The service ID we are connecting to on Fastly
- **Fastly API Token** - The API token used to access Fastly

### Suggestion

The settings, in particular the API token, should be carefully controlled and made as secure as possible.
If possible, look at using something like the [key](https://www.drupal.org/project/key) in combination with a key management service.

### Permissions

This module makes the permission `Access protected Lagoon routes` available.
Any user assigned this permission via roles will have their IP address recorded in the ACL when logging into the site.

