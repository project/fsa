<?php

namespace Drupal\fastly_streamline_access_admin\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\fastly_streamline_access\FsaFastly;
use Drupal\fastly_streamline_access\FsaFastlyDrupalUtilities;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class FsaAdminCommands extends DrushCommands {

  const AGE_OFF_STANDARD_NO_DAYS = 90;

  const AGE_OFF_EXTENDED_NO_DAYS = 270;

  protected $formattedOutputItemHeaders = [
    'ACL',
    'id',
    'IP Address',
    'Created At',
    'Subnet',
    'Negated',
    'Comment',
  ];

  /**
   * Constructs a FsaAdminCommands object.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Will remove any ips from Fastly older than 90 days and 6 months on long lived IPs.
   */
  #[CLI\Command(name: 'fastly_streamline_access:clear', aliases: ['fsac'])]
  #[CLI\Argument(name: 'ageOff', description: 'number of days after which to remove _all_ ACL entries - applies to Long Lived ACLs as well')]
  #[CLI\Option(name: 'allAcls', description: 'Summarize ACL information')]
  #[CLI\Usage(name: 'fastly_streamline_access:clear 90', description: 'Will remove any ips from Fastly older than 90 days and 6 months on long lived IPs')]
  public function cleanAcls($ageOff = NULL, $options = ['allAcls' => FALSE]) {
    $fastlyInterface = $this->getFastlyInterface();

    $acls = $this->getFsaAcls($fastlyInterface, $this->filterAcls($options));

    foreach ($acls as $acl) {
      $days = !is_null($ageOff) ? intval($ageOff) : self::AGE_OFF_STANDARD_NO_DAYS;

      if ($acl->name == \Drupal::config('fastly_streamline_access.settings')
          ->get('acl_long_name') && is_null($ageOff)) {
        $days = self::AGE_OFF_EXTENDED_NO_DAYS;
      }

      $this->cleanOldIpsFromAcl($fastlyInterface, $acl, $days);
    }
  }

  /**
   * Add Long Lived IP address.
   */
  #[CLI\Command(name: 'fastly_streamline_access:addLongLived', aliases: ['fsaal'])]
  #[CLI\Argument(name: 'ipaddress', description: 'IP Address to add to the long lived ACL')]
  #[CLI\Usage(name: 'fastly_streamline_access:addLongLived "ipaddress"', description: 'To add a subnet mask, use a slash ("192.0.2.0/24"). To exclude, use an exclamation mark ("!192.0.2.0").')]
  public function addLongLivedIp($ipaddress, $options = []) {
    $fastlyInterface = $this->getFastlyInterface();

    $acl = $fastlyInterface->getAclByName(
      \Drupal::config('fastly_streamline_access.settings')->get('acl_long_name')
    );

    if (!$acl) {
      $this->io()->error("Cannot find ACL - check settings and try again");
      return;
    }

    try {
      $ret = $fastlyInterface->addAclMember($acl->id, $ipaddress);
      if (isset($ret->created_at)) {
        $this->io()->writeln("Successfully added ip address");
      }
    } catch (\Exception $ex) {
      $this->io()->error($ex->getMessage());
      return;
    }
  }


  /**
   * Summarize ACL information.
   */
  #[CLI\Command(name: 'fastly_streamline_access:summary', aliases: ['fsasum'])]
  #[CLI\Option(name: 'allAcls', description: 'Summarize ACL information')]
  #[CLI\Usage(name: 'fastly_streamline_access:summary', description: 'Returns IP counts per ACL')]
  public function summarizeAcl($options = ['allAcls' => FALSE]): RowsOfFields {
    $rows = [];
    $fastlyInterface = $this->getFastlyInterface();
    foreach ($this->getFsaAcls($fastlyInterface, $this->filterAcls($options)) as $acl) {
      $count = count($fastlyInterface->getAclMembers($acl->id));
      $rows[] = ["ACL name" => $acl->name, "Count" => $count];
    }
    return new RowsOfFields($rows);
  }


  /**
   * Remove IP from all ACLs.
   */
  #[CLI\Command(name: 'fastly_streamline_access:remove', aliases: ['fsar'])]
  #[CLI\Argument(name: 'ipaddress', description: 'IP Address to add to the long lived ACL')]
  #[CLI\Option(name: 'allAcls', description: 'all ACLs, not just site\'s')]
  #[CLI\Usage(name: 'fastly_streamline_access:remove "ipaddress"', description: 'Will remove any ip W.X.Y.Z from Fastly')]
  public function removeIp($ipaddress, $options = ['allAcls' => FALSE]):RowsOfFields {
    $fastlyInterface = $this->getFastlyInterface();

    $rows["REMOVED FROM THE FOLLOWING ACCESS CONTROL LISTS"] = '';

    foreach (
      $this->getFsaAcls($fastlyInterface, $this->filterAcls($options)) as $acl
    ) {
      foreach ($fastlyInterface->getAclMembers($acl->id) as $ip) {
        if ($ip->ip == $ipaddress) {
          try {
            $fastlyInterface->deleteAclMember($acl->id, $ip->id);
          } catch (\Exception $ex) {
            $this->logger()->error($ex->getMessage());
            continue;
          }
          $rows[$acl->id] = $acl->name;
        }
      }
    }

    return new RowsOfFields($rows);
  }


  /**
   * Remove duplicate IPs from all ACLs.
   */
  #[CLI\Command(name: 'fastly_streamline_access:duplicates-remove', aliases: ['fsadr'])]
  #[CLI\Option(name: 'allAcls', description: 'all ACLs, not just site\'s')]
  #[CLI\Usage(name: 'fastly_streamline_access:duplicates-remove', description: 'Will remove any duplicated IP addresses from Fastly ACLs')]
  public function removeDuplicateIps($options = ['allAcls' => FALSE]) {
    return $this->removeDuplicateIpsDriver(FALSE, $options);
  }


  /**
   * Show duplicate IPs from all ACLs.
   */
  #[CLI\Command(name: 'fastly_streamline_access:duplicates-show', aliases: ['fsads'])]
  #[CLI\Option(name: 'allAcls', description: 'all ACLs, not just site\'s')]
  #[CLI\Usage(name: 'fastly_streamline_access:duplicates-show', description: 'Will show any duplicated IP addresses from Fastly ACLs')]
  public function showDuplicateIps($options = ['allAcls' => FALSE]) {
    return $this->removeDuplicateIpsDriver(TRUE, $options);
  }


  /**
   * Search for IP addresses in all ACLs.
   */
  #[CLI\Command(name: 'fastly_streamline_access:search', aliases: ['fsas'])]
  #[CLI\Argument(name: 'searchTerms', description: 'Target search text')]
  #[CLI\Option(name: 'allAcls', description: 'all ACLs, not just site\'s')]
  #[CLI\Usage(name: 'fastly_streamline_access:search "some text"', description: 'Will remove any ips from Fastly older than 90 days')]
  public function searchAcls($searchTerms = "", $options = ['allAcls' => FALSE]): RowsOfFields {
    $fastlyInterface = $this->getFastlyInterface();

    //run through all ACLs and IPs - search for matching terms
    $rows = [];
    $acls = $this->getFsaAcls($fastlyInterface, $this->filterAcls($options));

    foreach ($acls as $acl) {
      $ips = $fastlyInterface->getAclMembers($acl->id);
      foreach ($ips as $ip) {
        if (str_contains(json_encode($ip), $searchTerms)) {
          $formattedOutputItem = [
            'ACL' => $acl->name,
            'id' => $ip->id,
            'IP Address' => $ip->ip,
            'Created At' => $ip->created_at,
            'Subnet' => $ip->subnet,
            'Negated' => $ip->negated,
            'Comment' => $ip->comment,
          ];
          $rows[] = $formattedOutputItem;
        }
      }
    }
    $this->io()->title(
      \Drupal::translation()->translate("IPs containing '{$searchTerms}'")
    );
    return new RowsOfFields($rows);
  }

  /**
   * @param \Drupal\fastly_streamline_access\FsaFastly $fastlyInterface
   * @param $acl
   * @param $days
   */
  protected function cleanOldIpsFromAcl(
    FsaFastly $fastlyInterface,
              $acl,
              $days
  ): void {
    $now = new \DateTime();

    $ips = $fastlyInterface->getAclMembers($acl->id);
    foreach ($ips as $ip) {
      $createdDate = \DateTime::createFromFormat(
        "Y-m-d\TH:i:s\Z",
        $ip->created_at
      );

      $diff = $createdDate->diff($now);
      if ($diff->days > $days) {
        $fastlyInterface->deleteAclMember($acl->id, $ip->id);
        \Drupal::logger('fastly_streamline_access')->info(
          "Aging off ip '%ip' from ACL '%acl' because it is over %days days old",
          ['%ip' => $ip->ip, '%days' => $days, '%acl' => $acl->name]
        );
      }
    }
  }

  /**
   * @return \Drupal\fastly_streamline_access\FsaFastly
   * @throws \Exception
   */
  protected function getFastlyInterface(): FsaFastly {
    $fastlyInterface = FsaFastly::GetFsaFastlyInstance(
      FsaFastlyDrupalUtilities::getApiToken(),
      FsaFastlyDrupalUtilities::getServiceId()
    );
    return $fastlyInterface;
  }

  /**
   * Returns an array of the long and short ACL names registered with the
   * parent module
   *
   * @return array
   */
  protected function getRegisteredACLNames() {
    $config = \Drupal::config('fastly_streamline_access.settings');
    return [
      $config->get('acl_long_name'),
      $config->get('acl_name'),
    ];
  }

  /**
   * @param \Drupal\fastly_streamline_access\FsaFastly $fastlyInterface
   *
   * @return mixed
   */
  protected function getFsaAcls(
    FsaFastly $fastlyInterface,
    bool $filterAcls = TRUE
  ) {
    $acls = $fastlyInterface->getAclList();
    if (isset($acls->msg)) {
      $this->output()->writeln("FSA Error (" . $acls->msg . ") " . $acls->detail);
      return [];
    }
    $ourACLS = $this->getRegisteredACLNames();
    if ($filterAcls) {
      $acls = array_filter(
        $acls,
        function ($e) use ($ourACLS) {
          return in_array($e->name, $ourACLS);
        }
      );
    }
    return $acls;
  }

  /**
   *
   * @param $allAcls
   *
   * @return bool
   */
  protected function filterAcls($options): bool {
    return !empty($options['allAcls']) ? !$options['allAcls'] : TRUE;
  }

  /**
   * Remove duplicate IPs from all ACLs
   *
   *
   */
  private function removeDuplicateIpsDriver($dryRun = FALSE, $options = []) {
    $fastlyInterface = $this->getFastlyInterface();

    $aclMatches = [];

    foreach ($this->getFsaAcls($fastlyInterface, $this->filterAcls($options)) as $acl) {
      $uniqueIps = [];
      foreach ($fastlyInterface->getAclMembers($acl->id) as $ip) {
        if (in_array($ip->ip, $uniqueIps)) {
          try {
            if (!$dryRun) {
              $fastlyInterface->deleteAclMember($acl->id, $ip->id);
            }
          } catch (\Exception $ex) {
            $this->logger()->error($ex->getMessage());
            continue;
          }
          $count = !empty($aclMatches[$ip->ip]) ? $aclMatches[$ip->ip]["count"] + 1 : 1;
          $aclMatches[$ip->ip] = [
            "name" => $acl->name,
            "ip" => $ip->ip,
            "count" => $count,
          ];
        }
        else {
          $uniqueIps[] = $ip->ip;
        }
      }
    }

    $this->io()
      ->title(\Drupal::translation()->translate('IP ACL Removal Summary'));
    $this->io()->table(
      ["ACL", "IP Removed", "Number Duplicates"],
      $aclMatches
    );
  }

}
