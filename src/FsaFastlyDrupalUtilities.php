<?php

namespace Drupal\fastly_streamline_access;

/**
 * Convenience wrapper for some general Fastly functionality
 *
 * Class FsaFastlyDrupalUtilities
 *
 * @package Drupal\fastly_streamline_access
 */
class FsaFastlyDrupalUtilities {

  /**
   * @return array|false|string
   */
  public static function getServiceId() {
    $serviceID = \Drupal::config('fastly_streamline_access.settings')->get('service_id');

    if(empty($serviceID)) {
      throw new \Exception("FSA service id not set");
    }
    return $serviceID;
  }

  /**
   * @return array|false|string
   */
  public static function getApiToken() {
    $token = \Drupal::config('fastly_streamline_access.settings')->get('api_token');
    if(empty($token)) {
      throw new \Exception("API token not set");
    }
    return $token;
  }



}
