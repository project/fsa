<?php

namespace Drupal\fastly_streamline_access\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when we would like to negotiate FSA
 */
class FsaTriggerEvent extends Event {
  const EVENT_NAME = 'fsa_triggered';

}
