<?php

namespace Drupal\fastly_streamline_access\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class FsaConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ops_id_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('fastly_streamline_access.settings');

    // Page title field.
    $form['acl_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Standard Access Control List (ACL) name:'),
      '#default_value' => $config->get('acl_name'),
      '#description' => $this->t(
        'This will determine the ACL names used on Fastly'
      ),
    ];

    $form['acl_long_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Long lived Access Control List (ACL) name:'),
      '#default_value' => $config->get('acl_long_name'),
      '#description' => $this->t(
        'This will determine the ACL names used on Fastly'
      ),
    ];

    $form['service_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fastly Service ID'),
      '#default_value' => $config->get('service_id'),
      '#description' => $this->t(
        'This is the target Fastly Service ID'
      ),
    ];

    $form['api_token_fieldset'] = [
      '#type' => 'details',
      '#title' => t('api_token'),
      '#open' => empty($config->get('api_token')),
    ];

    if (!empty($config->get('api_token'))) {
      $form['api_token_fieldset']['override_api_token'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Override api_token:'),
        '#description' => $this->t(
          'The api_token is currently set - check here to override it'
        ),
      ];
    }

    // Page title field.
    $form['api_token_fieldset']['api_token'] = [
      '#type' => 'password',
      '#title' => $this->t('Api Token:'),
      '#default_value' => $config->get('api_token'),
      '#description' => $this->t(
        'The API token used to connect to Fastly'
      ),
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('fastly_streamline_access.settings');
    $config->set('acl_name', $form_state->getValue('acl_name'));
    $config->set('acl_long_name', $form_state->getValue('acl_long_name'));
    $config->set('service_id', $form_state->getValue('service_id'));

    if (empty($config->get('api_token')) || $form_state->getValue(
        'override_api_token'
      ) == 1) {
      $config->set('api_token', $form_state->getValue('api_token'));
    }
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['fastly_streamline_access.settings'];
  }

}
